const { test, expect} = require('@playwright/test');
test('KloudOne авторизация', async({page}) =>{
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByText('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.locator('#nextButton').click();
  await page.locator('input[name="password"]').fill('Vredinka1');
  await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  await page.locator('body').screenshot();
  //await page.getByText('Журнал').click();
  //await page.getByText('Войти').click();
});
test('KloudOne открытие вкладки Настройки подключения', async({page}) =>{
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByText('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.locator('#nextButton').click();
  await page.locator('input[name="password"]').fill('Vredinka1');
  await page.getByRole('button', { name: 'Войти', exact: true }).click();
  //await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  //await page.getByText('Войти').click()
  await page.getByText('Настройки подключения').click();
  //;
});

test('Настройки сценария', async ({ page }) => {
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByLabel('Введите логин, почту, телефон или id').click();
  await page.getByLabel('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.getByRole('button', { name: 'Далее' }).click();
  await page.getByRole('textbox').click();
  await page.getByRole('textbox').fill('Vredinka1');
  await page.getByRole('button', { name: 'Войти', exact: true }).click();
  await page.getByRole('button', { name: 'Настройки сценария' }).click();
  await page.getByRole('heading', { name: 'Настройки сценария ×' }).click();
});
test('Активация обзвона', async ({ page }) => {
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByLabel('Введите логин, почту, телефон или id').click();
  await page.getByLabel('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.getByRole('button', { name: 'Далее' }).click();
  await page.getByRole('textbox').click();
  await page.getByRole('textbox').fill('Vredinka1');
  await page.getByRole('button', { name: 'Войти', exact: true }).click();
  await page.getByRole('button', { name: 'Настройки сценария' }).click();
  await page.getByRole('button', { name: 'Интеграция с 1С ЖКХ' }).click();
  await page.getByRole('button', { name: 'Настройки сценария' }).click();
  //await page.getByRole('button', { name: 'Прослушать' }).click();
 /* await page.getByLabel('Месяцев задолженности').click();
  await page.getByLabel('Месяцев задолженности').fill('2');
  await page.getByLabel('Сумма задолженности').click();
  await page.getByLabel('Сумма задолженности').fill('5060');
  await page.getByRole('button', { name: 'Сохранить' }).click();*/
});  
test('Проверка sip-соединеия(валидные значения)', async ({ page }) => {
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByLabel('Введите логин, почту, телефон или id').click();
  await page.getByLabel('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.getByRole('button', { name: 'Далее' }).click();
  await page.getByRole('textbox').fill('Vredinka1');
  await page.getByRole('button', { name: 'Войти', exact: true }).click();
  await page.getByRole('button', { name: 'Настройки подключения' }).click();
  await page.getByLabel('Логин sip').click();
  //await page.getByLabel('Логин sip').fill('79297343141');
 /* await page.getByLabel('Пароль sip').click();
  await page.getByLabel('Пароль sip').fill('ddXDsjkzQ');
  await page.getByLabel('Адрес sip сервера').click();
  await page.getByLabel('Адрес sip сервера').fill('multifon.ru');
  await page.getByRole('button', { name: 'Проверка sip-соединения' }).click();
  await page.getByText('Статус сип-соединения: "зарегистрирован", время проверки: "2024.10.14 11:58:33"').click();*/
  await page.getByRole('button', { name: 'Проверка sip-соединения' }).click();
});
/*
test('KloudOne открытие вкладки Настройки сценария(откртие модального окна)', async({page}) =>{
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByText('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.locator('#nextButton').click();
  await page.locator('input[name="password"]').fill('Vredinka1');
  //await page.getByText()
  // await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  await page.getByText('Настройки сценария').click();
  //await page.getByText('Войти').click();
});
test('KloudOne открытие вкладки Настройки сценария Интеграция', async({page}) =>{
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByText('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.locator('#nextButton').click();
  await page.locator('input[name="password"]').fill('Vredinka1');
 // await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  await page.getByText('Настройки сценария').click();
  await page.getByText('Интеграция').click();
  //await page.getByText('Войти').click();
});

test('Подключение Sip-соединения', async ({ page }) => {
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByLabel('Введите логин, почту, телефон или id').click();
  await page.getByLabel('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.getByRole('button', { name: 'Далее' }).click();
  await page.getByRole('textbox').fill('Vredinka1');
  await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  //await page.getByRole('button', { name: 'Войти', exact: true }).click();
  await page.getByRole('button', { name: 'Настройки подключения' }).click();
  await page.getByLabel('Логин sip').click();
  await page.getByLabel('Логин sip').fill('79297343140');
  await page.getByLabel('Пароль sip').click();
  await page.getByLabel('Пароль sip').fill('ddXDsjkzQ');
  await page.getByLabel('Адрес sip сервера').click();
  await page.getByLabel('Адрес sip сервера').fill('multifon.ru');
  await page.getByLabel('Количество линий *').click();
  await page.getByLabel('Количество линий *').fill('01');
  await page.getByRole('button', { name: 'Сохранить' }).first().click();
});
test('KloudOne открытие вкладки Настройки сценария(Интеграция стрелочка)', async({page}) =>{
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByText('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.locator('#nextButton').click();
  await page.locator('input[name="password"]').fill('Vredinka1');
  await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  await page.getByText('Настройки сценария').click();
  await page.locator("xpath=//p[contains(text(),'Интеграция с 1С ЖКХ')]/ancestor::div[@id='panel1bh-header']/child::div[2]").click();
  await expect(page.locator("xpath=//span[contains(text(),'Активация')]")).toBeVisible();
  //await page.getByText('Войти').click();
});
test('Активация обзвона', async ({ page }) => {
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByLabel('Введите логин, почту, телефон или id').click();
  await page.getByLabel('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.getByRole('button', { name: 'Далее' }).click();
  await page.getByRole('textbox').click();
  await page.getByRole('textbox').fill('Vredinka1');
  await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  await page.getByRole('button', { name: 'Настройки сценария' }).click();
  await page.getByRole('button', { name: 'Интеграция с 1С ЖКХ' }).click();
  await page.getByRole('button', { name: 'Настройки сценария' }).click();
  await page.getByRole('button', { name: 'Прослушать' }).click();
  await page.getByLabel('Месяцев задолженности').click();
  await page.getByLabel('Месяцев задолженности').fill('2');
  await page.getByLabel('Сумма задолженности').click();
  await page.getByLabel('Сумма задолженности').fill('5000');
  //await page.getByText('Сохранить').click();
  await page.getByRole('button', { name: 'Настройки параметров обзвона' }).click();
  await page.getByText('Каждый день').click();
  //await page.getByRole('button', { name: 'Активировать' }).click();
  await page.getByRole('button', { name: 'OK' }).click();
});

test('KloudOne открытие вкладки Журнал', async({page}) =>{
  await page.goto('https://lkdev.etcd.kloud.one/obzvon/');
  await page.getByText('Введите логин, почту, телефон или id').fill('mikmarisha');
  await page.locator('#nextButton').click();
  await page.locator('input[name="password"]').fill('Vredinka1');
  await page.locator('button.MuiButtonBase-root.MuiButton-root.MuiButton-contained').click();
  await page.getByText('Журнал').click();
  //await page.getByText('Войти').click();
});

*/